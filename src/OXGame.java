
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class OXGame {
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static int turn = 0;
    static Scanner sc = new Scanner(System.in);
    
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");

    }

    static void showTable() {
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table.length; col++) {
                System.out.print(table[row][col]+" ");
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println( "turn "+ player );

    }

    static void input() {
        //normal flow
        while (true) {
            System.out.println("Please input Row Col: ");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            turn++;
//      System.out.println("row; "+row+" col; "+col);//debug
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: rable at row and col is not empty!!");
            showTurn();
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {
        if (table[0][0] == table[1][1] && table[1][1] == table[2][2]) {
            isFinish = true;
            winner = player;
        }
    }

    static void checkDraw() {
        if(turn == 9){
             isFinish = true;

        }
      
    }

    static void checkWin() {
            checkRow();
            checkCol();
            checkX();
            checkDraw();


    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(" >>>> "+winner + " winner <<<<");
        }
    }

    static void showBye() {
        System.out.println("Bye Bye ..... ");

    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
